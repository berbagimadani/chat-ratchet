<?php
namespace MyApp;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface; 

require dirname(__DIR__) . "../../client/app/config.php";

class ChatRoom implements WampServerInterface {
       

    private $dbh;
    protected $subscribedTopics = array(); 
    protected $clients;  



    public function __construct() { 
        global $dbh;
        $this->clients = new \SplObjectStorage;
        $this->dbh    = $dbh;   
     }
     


    public function onSubscribe(ConnectionInterface $conn, $topic) {
        $this->subscribedTopics[$topic->getId()] = $topic;
    }
     
    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public function onBlogEntry($entry) {
        $entryData = json_decode($entry, true);
     
        // If the lookup topic object isn't set there is no one to publish to
        if (!array_key_exists($entryData['all'], $this->subscribedTopics)) {
            return;
        }
     
        $topic = $this->subscribedTopics[$entryData['all']];
     
        // re-send the data to all the clients subscribed to that category
        $topic->broadcast($entryData);
    }

    /* The rest of our methods were as they were, omitted from docs to save space */
     
    public function onUnSubscribe(ConnectionInterface $conn, $topic) {
       
    }
    
    
    public function onOpen(ConnectionInterface $conn) {  
        
        //$this->clients->attach($conn); 
        //echo "New TES! ({$conn->resourceId})\n";  
 
        
        $conn->Chat        = new \StdClass; 
        $conn->Chat->name  = $conn->WAMP->sessionId;

        echo "sessID". $conn->Chat->name."\n";

         /*
        if (isset($conn->WebSocket)) {
            $conn->Chat->name = $this->escape($conn->WebSocket->request->getCookie('name'));

            if (empty($conn->Chat->name)) {
                $conn->Chat->name  = 'Anonymous ' . $conn->resourceId;
            }


            echo "tes name sock". $conn->Chat->name;
        } else {
            $conn->Chat->name  = 'Anonymous ' . $conn->resourceId;

            echo "tes name". $conn->Chat->name;
        }*/
    } 
 

    public function onClose(ConnectionInterface $conn) {
        
        //$this->clients->detach($conn); 
        //echo "Connection {$conn->resourceId} has disconnected\n";
         
        $sql = "DELETE FROM session WHERE id_session =  :id_session";
        $stmt = $this->dbh->prepare($sql); 
        $stmt->execute( array( ":id_session" => $conn->Chat->name ) );
        

        $query_update = $this->dbh->prepare('UPDATE users set status = ? where id_session = ? '); 
        $array_update = array('0', $conn->Chat->name);
        $query_update->execute($array_update);

        echo "closed=>".$conn->Chat->name;

        
    }

    protected function broadcast($topic, $msg, ConnectionInterface $exclude = null) {
        foreach ($this->rooms[$topic] as $client) {
            if ($client !== $exclude) {
                $client->event($topic, $msg);
            }
        }
    }
     
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }
     
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->close();
    }
     
    public function onError(ConnectionInterface $conn, \Exception $e) {
    }


 
}
?>