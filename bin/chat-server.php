<?php
use Ratchet\Server\IoServer;
use MyApp\Chat;

    require dirname(__DIR__) . '../vendor/autoload.php';


    $loop    = React\EventLoop\Factory::create();
    $pusher = new MyApp\Chat;

    $context = new React\ZMQ\Context($loop);

    $pull = $context->getSocket(ZMQ::SOCKET_PULL);
    $pull->bind('tcp://127.0.0.1:50555'); 
 
 	/*
    $server = IoServer::factory(
        new Chat(),
        8080
    );*/

    // Set up our WebSocket server for clients wanting real-time updates
	$webSock = new React\Socket\Server($loop);
	$webSock->listen(8086, '0.0.0.0'); // Binding to 0.0.0.0 means remotes can connect
	$webServer = new Ratchet\Server\IoServer(
	//new Ratchet\Wamp\WampServer(
		$pusher,
	//),
	$webSock
	);

	$loop->run();