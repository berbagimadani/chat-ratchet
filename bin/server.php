<?php

require dirname(__DIR__) . '../vendor/autoload.php';

// Create loop and socket
$loop   = React\EventLoop\Factory::create();
$socket = new React\Socket\Server($loop);

// New connection is established
$socket->on('connection', function (\React\Socket\ConnectionInterface $conn) {
    // New data arrives at the socket

    $conn->write("Hello there!\n");
    $conn->write("Welcome to this amazing server!\n");
    $conn->write("Here's a tip: don't say anything.\n");

    $conn->on('data', function ($data) use ($conn) {
        // TODO: Handle message
        echo "$data";
        // Close the connection when we consumed the message
        $conn->close();
    });
});

// The socket should listen to port 4000
$socket->listen(1337);
$loop->run();

?>