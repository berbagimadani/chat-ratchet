<?php

class Youtube {

    private $developer_key; 

    public function __construct( $developer_key  ) {
        
        $this->developer_key  = $developer_key; 
    }

    public function searchYoutube( $params ) {

        require_once '/../Google/autoload.php';
        require_once '/../Google/Client.php';
        require_once '/../Google/Service/YouTube.php';

          /*
           * Set $DEVELOPER_KEY to the "API key" value from the "Access" tab of the
           * {{ Google Cloud Console }} <{{ https://cloud.google.com/console }}>
           * Please ensure that you have enabled the YouTube Data API for your project.
           */
          $DEVELOPER_KEY = 'AIzaSyC8J9XjPXNSGqaWq1azRc-kh1rzETdM2Fg';

          $client = new Google_Client();
          $client->setDeveloperKey($DEVELOPER_KEY);

          // Define an object that will be used to make all API requests.
          $youtube = new Google_Service_YouTube($client);

          try {
            // Call the search.list method to retrieve results matching the specified
            // query term.
            $searchResponse = $youtube->search->listSearch( 'id,snippet', $params );

            $videos = '';
            $channels = '';
            $playlists = '';

            // Add each result to the appropriate list, and then display the lists of
            // matching videos, channels, and playlists.
            //foreach ($searchResponse['items'] as $searchResult) {
              

            //}

            
            return $searchResponse;

            } catch (Google_Service_Exception $e) {
               
               echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));

            } catch (Google_Exception $e) {
                
               echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
            }
    }


    public function videoYoutube( $params ) {

        require_once '/../Google/autoload.php';
        require_once '/../Google/Client.php';
        require_once '/../Google/Service/YouTube.php';

          /*
           * Set $DEVELOPER_KEY to the "API key" value from the "Access" tab of the
           * {{ Google Cloud Console }} <{{ https://cloud.google.com/console }}>
           * Please ensure that you have enabled the YouTube Data API for your project.
           */
          $DEVELOPER_KEY = 'AIzaSyC8J9XjPXNSGqaWq1azRc-kh1rzETdM2Fg';

          $client = new Google_Client();
          $client->setDeveloperKey($DEVELOPER_KEY);

          // Define an object that will be used to make all API requests.
          $youtube = new Google_Service_YouTube($client);

          try {
            // Call the search.list method to retrieve results matching the specified
            // query term.
            $searchResponse = $youtube->videos->listVideos( 'snippet,statistics,status', $params );

            $videos = '';
            $channels = '';
            $playlists = '';

            // Add each result to the appropriate list, and then display the lists of
            // matching videos, channels, and playlists.
            //foreach ($searchResponse['items'] as $searchResult) {
              

            //}

            
            return $searchResponse;

            } catch (Google_Service_Exception $e) {
               
               echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));

            } catch (Google_Exception $e) {
                
               echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
            }
    }

    public function pageNext() {

    }

    public function pagePrev() {
        
    }
     
}
 
 

?>