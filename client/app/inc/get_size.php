<?php
	
	function photosGetSize() {

        $params['api_key']          = $_GET["api_key"];
        $params['method']           ='flickr.photos.getSizes';
        $params['photo_id']         =  $_GET["id"];
        $params['format']           = 'json';
        $params['nojsoncallback']   = 1;

        $encoded_params = array();

        foreach ($params as $k => $v){
            $encoded_params[] = urlencode($k).'='.urlencode($v);
        }

        $url = "https://api.flickr.com/services/rest/?".implode('&', $encoded_params);
        $rsp = file_get_contents($url);
        
        $data = json_decode( $rsp, true); 
        $result = $data['sizes'];
       
       	print_r( json_encode( $result['size'] ) );
       
       /*foreach ( $result['size'] as $key => $value ) {
       		$source = null;
            if ( $value['label'] == 'Large Square' ) { 
            	$source .= $value['source'].',';
            }
            if ( $value['label'] == 'Large' ) { 
            	$source .= $value['source'].',';
            }
            if ( $value['label'] == 'Site MP4' ) { 
            	$source .= $value['source'].',';
            }		

            print( $source );  
       }*/

        
    } 
    
     function photosGetInfo() {

        $params['api_key']          = $_GET["api_key"];
        $params['method']           ='flickr.photos.getInfo';
        $params['photo_id']         =  $_GET["id"];
        $params['format']           = 'json';
        $params['nojsoncallback']   = 1;

        $encoded_params = array();

        foreach ($params as $k => $v){
            $encoded_params[] = urlencode($k).'='.urlencode($v);
        }

        $url = "https://api.flickr.com/services/rest/?".implode('&', $encoded_params);
        $rsp = file_get_contents($url);
        
        print_r($rsp); 
    } 

    photosGetSize();

?>