<?php 
namespace MyApp; 
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface; 
use Ratchet\pusher; 
require dirname(__DIR__) . "/client/app/config.php";

 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<script src="jquery-1.11.0.min.js"></script>
<script src="autobahn.min.js"></script> 
<script src="bootstrap.min.js"></script>

<?php
$userID = $_GET['user'];
/*
echo "<h2>User: ".$userID."</h2><br>";
echo "<span id='status'></span><br>"*/
?>
 
<div class='container-fluid'>
    
    <div>
        <?php
 

            $sql = $dbh->query("SELECT * from (SELECT * FROM `wsmessages` where to_user = '".$userID."' order by id desc limit 3 ) tmp order by tmp.id asc ");
            $msgs = $sql->fetchAll(); 

            $row = [];
            foreach ($msgs as $key => $value) {
                //echo "To User: ". $value["to_user"]."<br>";
                //echo "From User: ". $value["from_user"]."<br>";
                //echo "Subject:". $value["subject"]."<br>";

                $data['message']    =   $value["msg"];
                $data['from_user']    = $value["from_user"];

                //echo $value["msg"]." ( ".$value["from_user"]." ) <br><hr>";

                $row[] = $data;
            }

            print_r(json_encode($row));
        ?>
    </div>
    <div id='msg-push' class=''></div> 
</div>

 

<?php
 
//if($userID == '01'){
?>  
<script>
   
   
    
    
    var conn = new ab.Session('ws://<?php echo $ip; ?>:<?php echo $port; ?>',   
        function() {
            conn.subscribe('<?php echo $userID; ?>', function(topic, data) {
                // This is where you would add the new article to the DOM (beyond the scope of this tutorial)
                //var a = '<div class="btn-info col-xs-8">'+'To User : ' + topic + ' <br> From User: '+data.from_user+'<br> Subject: ' + data.subject+'<br>'+ data.msg+'<hr></div>'

                var a = '<div class="btn-info col-xs-8">'+data.msg+' ( '+data.from_user+' ) <br><hr></div>'

                console.log('sssssssss'+data);
                $('#msg-push').append(a);

            }); 
             
            sendStatus(conn._session_id, '<?php echo $userID; ?>');
            

            $('#status').html('ONLINE');
        },
        function() {
            console.warn(conn._session_id+'closed');
            deleteStatus(conn._session_id);
            $('#status').html('OFFLINE');
        },
        {'skipSubprotocolCheck': true} 
    ); 
     

    /*
    var conn = new WebSocket('ws://<?php echo $ip; ?>:8081');
    conn.onopen = function(e) {
        //console.log(e);
    };

    conn.onmessage = function(e) {
        console.log(e.data);
    };*/

    function sendStatus(session, user){

        $.post('../client/app/post-status.php',
            {
                session:session,
                user:user,
            },
            function(d){
                console.log(d);
            }
        );

    }
    function deleteStatus(id_session){

        $.post('../client/app/delete-status.php',
            {
                id_session:id_session, 
            },
            function(d){
                console.log(d);
            }
        );

    }
  

</script>

<?php //} ?>


 
</body>
</html>